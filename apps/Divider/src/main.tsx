import App from "./App";
import React from "react";
import { BrandProvider } from "./contexts/BrandContext";
import "./index.css";
import "@core/ds-foundations/nova.css";
import { createRoot } from "react-dom/client";

const container = document.getElementById("root");

if (container) {
  createRoot(container).render(
    <React.StrictMode>
      <BrandProvider brand="personal" theme="light">
        <App />
      </BrandProvider>
    </React.StrictMode>,
  );
}
