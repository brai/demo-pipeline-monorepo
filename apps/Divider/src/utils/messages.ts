type typeOfMessageType = "log" | "error" | "warn" | "info";
interface LoggerInterface {
  idMessage: number;
  typeOfMessage?: typeOfMessageType;
}
// En principio funciona como un console.xxx, la idea a futuro es llevarlo a que reciba fechas y
// path en donde se guarden archivos.
export const logger = ({
  idMessage,
  typeOfMessage = "log",
}: LoggerInterface) => {
  console[typeOfMessage](idMessage);
};
