/**
 * Returns the text type name based on the given index.
 * @param index - The index to determine the text type.
 * @returns The text type name.
 */
export const getTextTypeTextName = (index: number) => {
  switch (index) {
    case 0:
      return "volanta";
    case 2:
      return "bajada";
    case 3:
      return "parrafo";
    case 4:
      return "parrafoInfo";
  }
};
