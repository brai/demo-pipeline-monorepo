import { BrandType, ThemeType } from "./types/atoms/types";

export const BRANDPERSONAL: BrandType = "personal";
export const BRANDFLOW: BrandType = "flow";
export const THEMELIGHT: ThemeType = "light";
export const THEMEDARK: ThemeType = "dark";
export const SIZESOFT = "soft";
export const SIZEBOLD = "bold";
export const SIZEMEDIUM = "medium";
export const SIZEWHITE = "white";
export const ICONTYPEINFORMATIVE = "informative";
export const ICONTYPEINTERROGATIVE = "interrogative";

export const DIVIDERPERSONALLIGHTSIZE = [SIZESOFT, SIZEBOLD, SIZEMEDIUM];
export const DIVIDERPERSONALDARKSIZE = [SIZEWHITE];
export const DIVIDERFLOWDARKSIZE = [SIZESOFT, SIZEBOLD, SIZEMEDIUM];

const THEMELIGHTDARK = [THEMELIGHT, THEMEDARK];
const THEMEDARKLIGHT = [THEMEDARK, THEMELIGHT];
const THEMEDARKARRAY = [THEMEDARK];
const THEMELIGHTARRAY = [THEMELIGHT];

export const COMPONENTS = {
  ATOMS: {
    ARROW: "Arrow",
    BUTTON: "Button",
    CHECKBOX: "Checkbox",
    DIVIDER: {
      GENERAL: "DividerGeneral",
      SEMANTIC: "DividerSemantic",
      BRAND: "DividerBrand",
    },
    ICON: "Icon",
    INPUT: "Input",
    LABEL: "Label",
    RADIOBUTTON: "RadioButton",
    TAG: "Tag",
    TOGGLEBUTTON: "ToggleButton",
  },
  MOLECULES: {
    TEXT: "Text",
    TEXTFIELD: "TextField",
    TOOLTIP: "Tooltip",
  },
  ORGANISMS: {
    ACCORDION: "Accordion",
    JUMBOTRON: "Jumbotron",
  },
};

export const BRANDCONTEXTCOMPONENTS = [
  {
    component: COMPONENTS.ATOMS.ARROW,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.BUTTON,
    themesPersonal: THEMELIGHTDARK,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.CHECKBOX,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.DIVIDER.BRAND,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.DIVIDER.GENERAL,
    themesPersonal: THEMELIGHTDARK,
    themesFlow: THEMEDARKLIGHT,
  },
  {
    component: COMPONENTS.ATOMS.DIVIDER.SEMANTIC,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.RADIOBUTTON,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.TAG,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.ATOMS.TOGGLEBUTTON,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.MOLECULES.TEXT,
    themesPersonal: THEMELIGHTDARK,
    themesFlow: THEMEDARKARRAY,
  },
  {
    component: COMPONENTS.MOLECULES.TOOLTIP,
    themesPersonal: THEMELIGHTARRAY,
    themesFlow: THEMEDARKARRAY,
  },
];
