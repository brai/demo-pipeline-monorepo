import styled , { css }  from 'styled-components'
import { BrandType, DividerSemanticTokenColorType, DividerVariantSEWSemantictType, DividerVariantSemantictType, ThemeType } from '../../../../types/atoms/types'
import { ClassHR } from './classes'
import { PixelNumberType } from '../../Button/types'

const horizonalRow = new ClassHR()

export const DividerSemanticInterfaceSC = styled.hr<{ $brand: BrandType, $theme: ThemeType ,$token: DividerSemanticTokenColorType, $widthSize?:PixelNumberType}>`
${props => {
    const {$brand, $theme, $token} = props
    const [variant, size] = $token.split('-') as [DividerVariantSEWSemantictType, DividerVariantSemantictType]

    return(css`
        box-shadow: none;
        border: 1px solid ${horizonalRow.getColors($brand, $theme, variant, size)};
        width: ${props.$widthSize ?? '350px'};
    `)
    }}
`
