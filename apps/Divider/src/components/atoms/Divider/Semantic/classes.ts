import {
  BrandType,
  DividerSemanticBrandConstants,
  DividerSemanticThemeSemanticColors,
  DividerVariantSEWSemantictType,
  DividerVariantSemantictType,
  ThemeType,
  DividerSemanticSizeColors,
} from "../../../../types/atoms/types";

export class ClassHR {
  public CONSTANTS: DividerSemanticBrandConstants;

  constructor() {
    this.CONSTANTS = {
      personal: {
        light: {
          success: {
            soft: "var(--nv-sys-color-personal-light-divider-semantic-success-soft)",
            bold: "var(--nv-sys-color-personal-light-divider-semantic-success-bold)",
          },
          error: {
            soft: "var(--nv-sys-color-personal-light-divider-semantic-error-soft)",
            bold: "var(--nv-sys-color-personal-light-divider-semantic-error-bold)",
          },
          warning: {
            soft: "var(--nv-sys-color-personal-light-divider-semantic-warning-soft)",
            bold: "var(--nv-sys-color-personal-light-divider-semantic-warning-bold)",
          },
        },
      },
      flow: {
        dark: {
          success: {
            soft: "var(--nv-sys-color-flow-dark-divider-semantic-success-soft)",
            bold: "var(--nv-sys-color-flow-dark-divider-semantic-success-bold)",
          },
          error: {
            soft: "var(--nv-sys-color-flow-dark-divider-semantic-error-soft)",
            bold: "var(--nv-sys-color-flow-dark-divider-semantic-error-bold)",
          },
          warning: {
            soft: "var(--nv-sys-color-flow-dark-divider-semantic-warning-soft)",
            bold: "var(--nv-sys-color-flow-dark-divider-semantic-warning-bold)",
          },
        },
      },
    };
  }

  getColors(
    brand: BrandType,
    theme: ThemeType,
    variant: DividerVariantSEWSemantictType,
    size: DividerVariantSemantictType,
  ): string | undefined {
    let realValue;
    try {
      if (theme === "dark") {
        realValue =
          this.CONSTANTS[brand]?.dark?.[
            variant as keyof DividerSemanticThemeSemanticColors
          ]?.[size as keyof DividerSemanticSizeColors];
      } else {
        realValue =
          this.CONSTANTS[brand]?.light?.[
            variant as keyof DividerSemanticThemeSemanticColors
          ]?.[size as keyof DividerSemanticSizeColors];
      }
    } catch (e) {
      console.error(`Error in ClassHR.getColors: ${e}`);
    }
    return realValue !== undefined
      ? realValue
      : brand === "personal"
        ? "var(--nv-sys-color-personal-light-divider-semantic-success-soft)"
        : "var(--nv-sys-color-flow-dark-divider-semantic-success-soft)";
  }
}
