import { DividerSemanticPropsInterface } from "../../../../types/atoms/interfaces";
import { useContext } from "react";
import { BrandContext } from "../../../../contexts/BrandContext";
import { DividerSemanticInterfaceSC } from "./styledComponents";
import { COMPONENTS } from "../../../../constants";
import useThemeValidation from "../../../../hooks/useThemeValidation";

export const Semantic: React.FC<DividerSemanticPropsInterface> = ({
  token,
}) => {
  const { brand } = useContext(BrandContext);
  const { theme } = useThemeValidation(COMPONENTS.ATOMS.DIVIDER.SEMANTIC);
  return (
    <DividerSemanticInterfaceSC $theme={theme} $brand={brand} $token={token} />
  );
};
