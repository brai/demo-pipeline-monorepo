import { BRANDPERSONAL } from "../../../../constants";
import { DividerGeneralClassInterface } from "../../../../types/atoms/interfaces";
import {
  BrandType,
  DividerVariantType,
  ThemeType,
} from "../../../../types/atoms/types";

export class ClassHR {
  public CONSTANTS: DividerGeneralClassInterface;

  constructor() {
    this.CONSTANTS = {
      personal: {
        light: {
          soft: "var(--nv-sys-color-personal-light-divider-general-soft)",
          medium: "var(--nv-sys-color-personal-light-divider-general-medium)",
          bold: "var(--nv-sys-color-personal-light-divider-general-bold)",
        },
        dark: {
          soft: "var(--nv-sys-color-flow-light-divider-general-soft)",
        },
      },
      flow: {
        dark: {
          soft: "var(--nv-sys-color-flow-dark-divider-general-soft)",
          medium: "var(--nv-sys-color-flow-dark-divider-general-medium)",
          bold: "var(--nv-sys-color-flow-dark-divider-general-bold)",
        },
        light: {
          soft: "var(--nv-sys-color-flow-light-divider-general-soft)",
        },
      },
    };
  }

  getColors(
    brand: BrandType,
    theme: ThemeType | string,
    size: DividerVariantType | string,
  ) {
    let realValue;
    if (brand === "personal" && theme === "dark" && size !== "soft") {
      console.warn(
        `Error en Divider Personal. ClassHR.getColors: No existe la variante ${size} en el tema ${theme} del brand ${brand}. Se asignará la variante soft.`,
      );
      size = "soft";
    }
    if (brand === "flow" && theme === "light" && size !== "soft") {
      console.warn(
        `Error en Divider Flow. ClassHR.getColors: No existe la variante ${size} en el tema ${theme} del brand ${brand}. Se asignará la variante soft.`,
      );
      size = "soft";
    }
    try {
      realValue = this.CONSTANTS[brand][theme][size];
    } catch (e) {
      console.error(`Error en Divider General. ClassHR.getColors: ${e}`);
    }
    console.log(realValue);
    return realValue !== undefined
      ? realValue
      : brand === BRANDPERSONAL
        ? "var(--nv-sys-color-personal-light-divider-general-soft)"
        : "var(--nv-sys-color-flow-dark-divider-general-bold)";
  }
}
