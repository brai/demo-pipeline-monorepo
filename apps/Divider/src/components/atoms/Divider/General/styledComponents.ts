/**
 * DividerGeneralInterfaceSC is a styled component representing a general divider interface.
 *
 * @remarks
 * It accepts the following props:
 * - $brand: The brand type for the divider.
 * - $theme: The theme type for the divider.
 * - $token: The token type for the divider.
 * - $widthSize: The width size of the divider (optional).
 *
 * @example
 * ```tsx
 * <DividerGeneralInterfaceSC
 *   $brand="primary"
 *   $theme="light"
 *   $token="solid"
 *   $widthSize="500px"
 * />
 * ```
 *
 * @param props - The component props.
 * @returns The styled hr element representing the general divider interface.
 */
import styled, { css } from "styled-components";
import {
  BrandType,
  DividerGeneralPropsType,
  ThemeType,
} from "../../../../types/atoms/types";
import { ClassHR } from "./classes";
import { PixelNumberType, PorcentualNumberType } from "../../Button/types";

const horizonalRow = new ClassHR();

export const DividerGeneralInterfaceSC = styled.hr<{
  $brand: BrandType;
  $theme: ThemeType;
  $token: DividerGeneralPropsType;
  $widthSize?: PixelNumberType | PorcentualNumberType;
}>`
  ${(props) => {
    const { $brand, $theme, $token } = props;
    return css`
      box-shadow: none;
      border: 1px solid ${horizonalRow.getColors($brand, $theme, $token)};
      width: ${props.$widthSize ?? "350px"};
    `;
  }}
`;
