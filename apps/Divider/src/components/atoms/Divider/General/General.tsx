import { DividerGeneralPropsInterface } from "../../../../types/atoms/interfaces";
import { useContext } from "react";
import { BrandContext } from "../../../../contexts/BrandContext";
import { DividerGeneralInterfaceSC } from "./styledComponents";
import { COMPONENTS } from "../../../../constants";
import useThemeValidation from "../../../../hooks/useThemeValidation";

/**
 * Renders a general divider component.
 *
 * @component
 * @example
 * ```tsx
 * <General token="tokenValue" widthSize={2} />
 * ```
 *
 * @param {DividerGeneralPropsInterface} props - The props for the General component.
 * @param {string} props.token - The token value for the divider.
 * @param {number} props.widthSize - The width size of the divider.
 * @returns {JSX.Element} The rendered General component.
 */

export const General: React.FC<DividerGeneralPropsInterface> = ({
  token,
  widthSize,
}) => {
  const { brand } = useContext(BrandContext);
  const { theme } = useThemeValidation(COMPONENTS.ATOMS.DIVIDER.GENERAL);
  return (
    <DividerGeneralInterfaceSC
      $theme={theme}
      $brand={brand}
      $token={token}
      $widthSize={widthSize}
    />
  );
};
