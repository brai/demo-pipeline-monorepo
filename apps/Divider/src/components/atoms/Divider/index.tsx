import { Brand } from "./Brand/Brand";
import { General } from "./General/General";
import { Semantic } from "./Semantic/Semantic";

/**
 * Props for the Divider component.
 */
interface DividerProps {
  children: React.ReactNode[];
}

/**
 * A customizable divider component.
 * @param children - The content to be rendered within the divider.
 * @returns The rendered divider component.
 */
const Divider = ({ children }: DividerProps) => {
  return <>{children}</>;
};

// Expose sub-components as properties of the Divider component
Divider.Brand = Brand;
Divider.General = General;
Divider.Semantic = Semantic;

export default Divider;
