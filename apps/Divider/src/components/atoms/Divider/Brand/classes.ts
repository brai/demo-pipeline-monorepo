import { BRANDPERSONAL } from "../../../../constants";
import {
  BrandType,
  DividerVarianBrandtType,
  DividerVariantPSTBrandType,
  ThemeType,
} from "../../../../types/atoms/types";

type DividerColors = {
  primary: {
    soft: string;
    medium: string;
    bold: string;
  };
  secondary: {
    soft: string;
    medium: string;
    bold: string;
  };
  tertiary: {
    soft: string;
    medium: string;
    bold: string;
  };
};

type BrandThemes = {
  light?: DividerColors;
  dark?: DividerColors;
};

type BrandConstants = {
  personal: BrandThemes;
  flow: BrandThemes;
};

export class ClassHR {
  CONSTANTS: BrandConstants;

  constructor() {
    this.CONSTANTS = {
      personal: {
        light: {
          primary: {
            soft: "var(--nv-sys-color-personal-light-divider-brand-primary-soft)",
            medium:
              "var(--nv-sys-color-personal-light-divider-brand-primary-medium)",
            bold: "var(--nv-sys-color-personal-light-divider-brand-primary-bold)",
          },
          secondary: {
            soft: "var(--nv-sys-color-personal-light-divider-brand-secondary-soft)",
            medium:
              "var(--nv-sys-color-personal-light-divider-brand-secondary-medium)",
            bold: "var(--nv-sys-color-personal-light-divider-brand-secondary-bold)",
          },
          tertiary: {
            soft: "var(--nv-sys-color-personal-light-divider-brand-tertiary-soft)",
            medium:
              "var(--nv-sys-color-personal-light-divider-brand-tertiary-medium)",
            bold: "var(--nv-sys-color-personal-light-divider-brand-tertiary-bold)",
          },
        },
      },
      flow: {
        dark: {
          primary: {
            soft: "var(--nv-sys-color-flow-dark-divider-brand-primary-soft)",
            medium:
              "var(--nv-sys-color-flow-dark-divider-brand-primary-medium)",
            bold: "var(--nv-sys-color-flow-dark-divider-brand-primary-bold)",
          },
          secondary: {
            soft: "var(--nv-sys-color-flow-dark-divider-brand-secondary-soft)",
            medium:
              "var(--nv-sys-color-flow-dark-divider-brand-secondary-medium)",
            bold: "var(--nv-sys-color-flow-dark-divider-brand-secondary-bold)",
          },
          tertiary: {
            soft: "var(--nv-sys-color-flow-dark-divider-brand-tertiary-soft)",
            medium:
              "var(--nv-sys-color-flow-dark-divider-brand-tertiary-medium)",
            bold: "var(--nv-sys-color-flow-dark-divider-brand-tertiary-bold)",
          },
        },
      },
    };
  }

  getColors(
    brand: BrandType,
    theme: ThemeType,
    variant: DividerVariantPSTBrandType,
    size: DividerVarianBrandtType,
  ) {
    try {
      const realValue = this.CONSTANTS[brand][theme]?.[variant]?.[size];
      if (realValue) return realValue;
    } catch (e) {
      console.error(`Error in Divider Brand. ClassHR.getColors: ${e}`);
    }
    return brand === BRANDPERSONAL
      ? "var(--nv-sys-color-personal-light-divider-brand-primary-soft)"
      : "var(--nv-sys-color-flow-dark-divider-brand-primary-soft)";
  }
}
