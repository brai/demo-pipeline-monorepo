import styled, { css } from "styled-components";
import {
  BrandType,
  DividerBrandTokenColorType,
  DividerVarianBrandtType,
  DividerVariantPSTBrandType,
  ThemeType,
} from "../../../../types/atoms/types";
import { ClassHR } from "./classes";
import { PixelNumberType } from "../../Button/types";

const horizontalRow = new ClassHR();

/**
 * DividerBrandInterfaceSC is a styled component representing a brand-themed divider.
 *
 * @remarks
 * This component accepts the following props:
 * - $brand: The brand type for the divider.
 * - $theme: The theme type for the divider.
 * - $token: The token color type for the divider.
 * - $widthSize (optional): The width size for the divider.
 */
export const DividerBrandInterfaceSC = styled.hr<{
  $brand: BrandType;
  $theme: ThemeType;
  $token: DividerBrandTokenColorType;
  $widthSize?: PixelNumberType;
}>`
  ${(props) => {
    const { $brand, $theme, $token, $widthSize } = props;
    const [variant, size] = $token.split("-") as [
      DividerVariantPSTBrandType,
      DividerVarianBrandtType,
    ];

    return css`
      box-shadow: none;
      border: 1px solid
        ${horizontalRow.getColors($brand, $theme, variant, size)};
      width: ${$widthSize ?? "350px"};
    `;
  }}
`;
