import { DividerBrandPropsInterface } from "../../../../types/atoms/interfaces";
import { DividerBrandInterfaceSC } from "./styledComponents";
import { useBrand } from "../../../../hooks/useBrand";
import { COMPONENTS } from "../../../../constants";
import useThemeValidation from "../../../../hooks/useThemeValidation";

/**
 * Renders the brand component for the divider.
 *
 * @param {DividerBrandPropsInterface} props - The props for the brand component.
 * @param {string} props.token - The token for the brand component.
 * @returns {JSX.Element} The rendered brand component.
 */
export const Brand: React.FC<DividerBrandPropsInterface> = ({ token }) => {
  const { brand } = useBrand();
  const { theme } = useThemeValidation(COMPONENTS.ATOMS.DIVIDER.BRAND);
  return (
    <DividerBrandInterfaceSC $brand={brand} $theme={theme} $token={token} />
  );
};
