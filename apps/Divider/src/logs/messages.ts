/**
 * Represents a collection of warning messages.
 */
export const warn: {
  [key: number]: (valueSearched: string, array: string[]) => string;
} = {
  /**
   * Returns a warning message for a specific value searched in an array.
   * @param valueSearched - The value being searched.
   * @param array - The array being searched in.
   * @returns A warning message indicating that the value is not included in the array.
   */
  1: (valueSearched: string, array: string[]) => {
    return `${valueSearched} no está incluida en los valores: ${array.join(", ")}`;
  },
};

/**
 * Returns the warning message function for a specific ID.
 * @param id - The ID of the warning message function.
 * @returns The warning message function for the specified ID.
 */
export const getMessageWarning = (id: number) => {
  return warn[id];
};
