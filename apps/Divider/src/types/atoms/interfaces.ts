import { ReactNode } from "react";
import {
  BrandType,
  DividerTokenColorType,
  DividerGeneralPropsType,
  DividerBrandTokenColorType,
  DividerSemanticTokenColorType,
  ThemeType,
  PixelNumberType,
  PorcentualNumberType,
} from "./types";

export interface CheckboxPropsInterface {
  checked: boolean;
  onChange: (checked: boolean) => void;
  label?: string;
}

export interface RadioButtonPropsInterface {
  selected: boolean;
  onChange: (checked: boolean) => void;
  label?: string;
  name: string;
}

export type ToggleButtonProps = {
  turnon: boolean;
  children?: ReactNode;
  onClick?: () => void;
};

export interface DividerProps {
  token: DividerTokenColorType;
}

export interface DividerGeneralPropsInterface {
  token: DividerGeneralPropsType;
  setTheme?: (theme: ThemeType) => void;
  widthSize?: PixelNumberType | PorcentualNumberType;
}

export interface DividerBrandPropsInterface {
  token: DividerBrandTokenColorType;
}

export interface DividerSemanticPropsInterface {
  token: DividerSemanticTokenColorType;
}

export interface ThemeComponentInterface {
  component: string;
  personal: ThemeType[];
  flow: ThemeType[];
}

export interface BrandProviderPropsInterface {
  children: ReactNode;
  brand?: BrandType;
  theme?: ThemeType;
}

export interface DividerGeneralClassInterface {
  [key: string]: {
    [key: string]: {
      [key: string]: string;
    };
  };
}
