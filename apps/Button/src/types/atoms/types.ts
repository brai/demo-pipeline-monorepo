import { DividerVariantEnum } from "./enums";
import { ThemeComponentInterface } from "./interfaces";

export type BrandType = "personal" | "flow";
export type ThemeType = "light" | "dark";

export type DefaultThemeInterface = ThemeComponentInterface[];
export type DividerVariantType = 'soft' | 'medium' | 'bold' | 'white' | 'gray';
export type DividerBrandTypeMap = { [K in BrandType]: K; };
export type DividerGeneralPropsType = 'soft' | 'medium' | 'bold' ;
export type DividerVariantPSTBrandType = 'primary' | 'secondary' | 'tertiary'
export type DividerVarianBrandtType = 'soft' | 'medium' | 'bold' 
export type DividerVariantSEWSemantictType = 'success' | 'error' | 'warning' 
export type DividerVariantSemantictType = 'soft' | 'bold' 
export type DividerTokenColorType = 'personal-light-soft' | 'personal-light-medium' | 'personal-light-bold' | 'personal-dark-white' | 'flow-dark-white' | 'flow-dark-soft' | 'flow-dark-medium' | 'flow-dark-bold';
export type DividerBrandTokenColorType = 'primary-medium' | 'primary-bold' | 'secondary-soft' | 'secondary-medium' | 'secondary-bold' | 'tertiary-soft' | 'tertiary-medium' | 'tertiary-bold' | 'primary-soft' | 'primary-medium' | 'primary-bold' | 'secondary-soft' | 'secondary-medium' | 'secondary-bold' | 'tertiary-soft' | 'tertiary-medium' | 'tertiary-bold'
export type DividerSemanticTokenColorType = 'success-soft' | 'success-bold' | 'error-soft' | 'error-bold' | 'warning-soft' |  'warning-bold'
export type EnumDictionary = Record<DividerVariantEnum, string>;
export type PixelNumberType = `${number}px`
export type PorcentualNumberType = `${number}%`;
export type TextTextPropType = "volanta" | "titulo" | "bajada" | "parrafo" | "parrafoInfo";
export type ToggleButtonBaseType = 'container' | 'button'
export type ToggleButtonTokenType = 'light' | 'dark'

export type ComponentThemeConfig = {
    component: string;
    themesPersonal: ThemeType[];
    themesFlow: ThemeType[];
};
export type BrandContextType = {
    brand: BrandType;
    theme: ThemeType;
    setBrand: (brand: BrandType) => void;
    setTheme: (theme: ThemeType) => void;
    componentThemes: ComponentThemeConfig[];
};
export type DividerSemanticSizeColors = {
    soft: string;
    bold: string;
};

export type DividerSemanticThemeSemanticColors = {
    success: DividerSemanticSizeColors;
    error: DividerSemanticSizeColors;
    warning: DividerSemanticSizeColors;
};

type DividerSemanticThemeTypeColors = {
    light?: DividerSemanticThemeSemanticColors;
    dark?: DividerSemanticThemeSemanticColors;
};

export type DividerSemanticBrandConstants = {
    personal: DividerSemanticThemeTypeColors;
    flow: DividerSemanticThemeTypeColors;
};
