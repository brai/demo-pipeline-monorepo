export enum DividerVariantEnum {
    SOFT = 'soft',
    MEDIUM = 'medium',
    BOLD = "bold",
    WHITE = 'white'
}
