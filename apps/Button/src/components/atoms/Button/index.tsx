
import { ButtonProps } from "./interfaces";
import { StyledButton } from "./styledComponents";
import { SubvariantType } from "./types";
import { useBrand } from "../../../hooks/useBrand";
import useThemeValidation from "../../../hooks/useThemeValidation";
import { COMPONENTS } from "../../../constants";

/**
 * ComponentName: Button
 *
 * Description: A customizable button component that adapts to different themes, brands, and screen sizes.
 *
 * @author Fernando del Valle <fmdelvalle@teco.com.ar>
 * @creationDate 2024-07-21
 * @version 1.0.0
 *
 * @param {string} tokenColor - The color token to use for the button, default is 'primary-standard'.
 * @param {string} size - The size of the button, default is 'large'.
 * @param {React.ReactNode} children - The content to be displayed inside the button, default is "Push Me".
 * @param {Function} onClick - The click handler for the button, default is a console log function.
 * @param {Object} rest - Any additional props to be passed to the button.
 *
 * @returns {JSX.Element} The rendered button component.
 *
 * @example
 * <Button
 *   tokenColor="secondary-standard"
 *   size="medium"
 *   onClick={() => alert("Button clicked!")}>
 *   Click Me
 * </Button>
 *
 * @jiraTicket #TCD-10482
 */

const Button = ({
  tokenColor = "primary-standard",
  size = "large",
  children = "Push Me",
  onClick = () => alert("Boton"),
  ...rest
}: ButtonProps) => {
  const subvariant = tokenColor.split("-")[1] as SubvariantType;

  const { brand } = useBrand();
  const { theme } = useThemeValidation(COMPONENTS.ATOMS.BUTTON);

  return (
    <StyledButton
      $brand={brand}
      $theme={theme}
      $tokenColor={tokenColor}
      $device='desktop'
      $size={size}
      onClick={subvariant !== "disabled" ? onClick : () => undefined}
      {...rest}
    >
      {children}
    </StyledButton>
  );
};
export default Button;
