import { useState } from "react";
import { BrandType, ThemeType } from "../../../types/atoms/types";
import { ActionType, VariantType } from "./types";

export const useTokenColor = (token: string) => {
  const tokenSplit = token.split("-");
  // eslint-disable-next-line
  const [brand, setBrand] = useState<BrandType>(tokenSplit[0] as BrandType);
  // eslint-disable-next-line
  const [theme, setTheme] = useState<ThemeType>(tokenSplit[1] as ThemeType);
  // eslint-disable-next-line
  const [variant, setVariant] = useState<VariantType>(
    tokenSplit[2] as VariantType,
  );
  // eslint-disable-next-line
  const [action, setAction] = useState<ActionType>(tokenSplit[3] as ActionType);

  return { brand, theme, variant, action };
};
