import { Meta, StoryObj } from '@storybook/react';
import Button from './index';
import useThemeValidation from '../../../hooks/useThemeValidation';
import { useBrand } from '../../../hooks/useBrand';
import React from 'react';

const withReactMountHook = (Story, context) => {
    const { theme } = useThemeValidation('Button');
    const {setBrand} = useBrand()
  
    setBrand(context.globals.brand);
  
    return <Story {...context} args={{ ...context.args, theme }} />;
  };

const meta: Meta<typeof Button> = {
    title: 'DS/Components/Atoms/Button',
    component: Button,
    parameters: {
        layout: 'centered',
    },
    decorators: [withReactMountHook],
    tags: ['autodocs'],
    
};

export default meta;

type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        tokenColor: 'primary-standard',
        size: 'small',
        children: 'Botón',
        onClick: () => alert('Botonazo clickeado!'),
    },
};

export const Secondary: Story = {
    args: {
        tokenColor: 'secondary-standard',
        size: 'small',
        children: 'Botón',
        onClick: () => alert('Botonazo clickeado!'),
    },
};

export const Tertiary: Story = {
    args: {
        tokenColor: 'tertiary-standard',
        size: 'small',
        children: 'Botón',
        onClick: () => alert('Botonazo clickeado!'),
    },
};

export const Disabled: Story = {
    args: {
        disabled: true,
        size: 'small',
        children: 'Botón',
        onClick: () => alert('Botonazo clickeado!'),
    },
};
