// import { ValidColorsType } from "@core/ds-icons/dist/ValidValuesType";
import {
  ActionType,
  AttributeType,
  BrandType,
  DeviceType,
  SizeDeviceType,
  SubvariantType,
  ThemeType,
  VariantType,
} from "./types";
import { ButtonClassMeasuresMatrixType } from "./interfaces";
import { BRANDPERSONAL } from "../../../constants";

export class ClassButton {
  public MATRIX: ButtonClassMeasuresMatrixType;

  constructor() {
    this.MATRIX = {
      measures: {
        desktop: {
          small: {
            height: "48px",
            widthWidthOutIcon: "91px",
            fontSize: "16px",
            charactersCount: {
              linesByText: 1,
              min: 5,
              max: 20,
            },
            iconPosition: "left",
            iconAlign: "center",
            iconSize: "16px",
            borderRadius: "4px",
            borderRadiusFocus: "6px",
            marginLeft: "24px",
            marginRight: "24px",
            marginTop: "12px",
            marginBottom: "12px",
            gap: "8px",
            /*
                            borderInside: 2px;
                            borderOutside: 2px;
                            heightFocus: 56px;
                        */
          },
          medium: {
            height: "48px",
            widthWidthOutIcon: "224px",
            fontSize: "16px",
            charactersCount: {
              linesByText: 1,
              min: 5,
              max: 20,
            },
            iconPosition: "left",
            iconAlign: "center",
            iconSize: "16px",
            borderRadius: "4px",
            borderRadiusFocus: "6px",
            marginLeft: "24px",
            marginRight: "24px",
            marginTop: "12px",
            marginBottom: "12px",
            gap: "8px",
            /*
                            borderInside: 2px;
                            borderOutside: 2px;
                            heightFocus: 56px;
                        */
          },
          large: {
            height: "48px",
            widthWidthOutIcon: "320px",
            fontSize: "16px",
            charactersCount: {
              linesByText: 1,
              min: 5,
              max: 20,
            },
            iconPosition: "left",
            iconAlign: "center",
            iconSize: "16px",
            borderRadius: "4px",
            borderRadiusFocus: "8px",
            marginLeft: "24px",
            marginRight: "24px",
            marginTop: "12px",
            marginBottom: "12px",
            gap: "8px",
            /*
                            borderInside: 2px;
                            borderOutside: 2px;
                            heightFocus: 56px;
                        */
          },
        },
        mobile: {
          small: {
            height: "40px",
            widthWidthOutIcon: "75px",
            fontSize: "16px",
            charactersCount: {
              linesByText: 1,
              min: 5,
              max: 20,
            },
            iconPosition: "left",
            iconAlign: "center",
            iconSize: "16px",
            borderRadius: "4px",
            borderRadiusFocus: "6px",
            marginLeft: "16px",
            marginRight: "16px",
            marginTop: "8px",
            marginBottom: "8px",
            gap: "8px",
            /*
                            borderInside: 2px;
                            borderOutside: 2px;
                            heightFocus: 48px;
                        */
          },
          medium: {
            height: "40px",
            widthWidthOutIcon: "224px",
            fontSize: "16px",
            charactersCount: {
              linesByText: 1,
              min: 5,
              max: 20,
            },
            iconPosition: "left",
            iconAlign: "center",
            iconSize: "16px",
            borderRadius: "4px",
            borderRadiusFocus: "6px",
            marginLeft: "16px",
            marginRight: "16px",
            marginTop: "8px",
            marginBottom: "8px",
            gap: "8px",
            /*
                            borderInside: 2px;
                            borderOutside: 2px;
                            heightFocus: 48px;
                        */
          },
          large: {
            height: "40px",
            widthWidthOutIcon: "320px",
            fontSize: "16px",
            charactersCount: {
              linesByText: 1,
              min: 5,
              max: 20,
            },
            iconPosition: "left",
            iconAlign: "center",
            iconSize: "16px",
            borderRadius: "4px",
            borderRadiusFocus: "6px",
            marginLeft: "16px",
            marginRight: "16px",
            marginTop: "8px",
            marginBottom: "8px",
            gap: "8px",
            /*
                            borderInside: 2px;
                            borderOutside: 2px;
                            heightFocus: 48px;
                        */
          } /*
                    icon: {
                        // FALTA VER ESTE
                    } */,
        },
      },
      colors: {
        personal: {
          light: {
            primary: {
              standard: {
                enabled: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-standard-enabled)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              delete: {
                enabled: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-delete-enabled)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              success: {
                enabled: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-success-enabled)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-success-hover)",
                  color:
                    "var--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-primary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              disabled: {
                background:
                  "var(--nv-sys-color-personal-light-background-button-primary-standard-disabled)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-primary-disabled-button-bg)",
                cursor: "not-allowed",
                border: "none",
              },
            },
            secondary: {
              standard: {
                enabled: {
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-standard)",
                  background: "transparent",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-standard)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-standard)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  background: "transparent",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-standard)",
                },
              },
              delete: {
                enabled: {
                  background: "transparent",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-delete)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-delete-button-bg)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-delete-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-delete)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-delete-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-delete)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-delete)",
                },
              },
              success: {
                enabled: {
                  background: "transparent",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-success-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-success)",
                },
                hover: {
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-success)",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-success-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-success-button-bg)",
                },
                pressed: {
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-success)",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-secondary-success-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              neutral: {
                enabled: {
                  background: "transparent",
                  color:
                    "var(--nv-sys-color-personal-light-typography-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-neutral)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-neutral-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-typography-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-neutral)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-secondary-neutral-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-typography-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-neutral)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  background: "transparent",
                  border:
                    "1px solid var(--nv-sys-color-personal-light-border-button-secondary-neutral)",
                  color:
                    "var(--nv-sys-color-personal-light-typography-text-icon-on-secondary-neutral-button-bg)",
                },
              },
              disabled: {
                background:
                  "var(--nv-sys-color-personal-light-background-button-primary-standard-disabled)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-secondary-disabled-button-bg)",
                cursor: "not-allowed",
                border: "none",
              },
            },
            tertiary: {
              standard: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-standard-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-standard-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-standard-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              delete: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-delete-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-delete-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-delete-button-bg)",
                },
                focus: {
                  border: "none",
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              success: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-success-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-success-hover)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-success-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-success-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  border: "none",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              neutral: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-neutral-button-bg)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-neutral-hover)",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-neutral-button-bg)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-light-background-button-tertiary-neutral-pressed)",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-light-text-icon-on-tertiary-neutral-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  border: "none",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              disabled: {
                background: "transparent",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tertiary-disabled-button-bg)",
                cursor: "not-allowed",
                border: "none",
              },
            },
          },
          dark: {
            primary: {
              standard: {
                enabled: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-standard-enabled)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              delete: {
                enabled: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-delete-enabled)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              success: {
                enabled: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-success-enabled)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-success-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-primary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border: "none",
                },
              },
              disabled: {
                background:
                  "var(--nv-sys-color-personal-light-background-button-primary-standard-disabled)",
                color:
                  "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                cursor: "not-allowed",
                border: "none",
              },
            },
            secondary: {
              standard: {
                enabled: {
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-standard)",
                  background: "transparent",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-standard)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-standard)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  background: "none",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-standard)",
                },
              },
              delete: {
                enabled: {
                  background: "transparent",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-delete)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-delete-button-bg)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-delete-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-delete)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-delete-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-delete)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-delete)",
                },
              },
              success: {
                enabled: {
                  background: "transparent",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-success-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-success)",
                },
                hover: {
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-success)",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-success-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-success-button-bg)",
                },
                pressed: {
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-success)",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-success-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              neutral: {
                enabled: {
                  background: "transparent",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-neutral)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-neutral-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-neutral)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-secondary-neutral-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-neutral)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              disabled: {
                background: "transparent",
                color:
                  "var(--nv-sys-color-personal-dark-text-icon-on-secondary-disabled-button-bg)",
                cursor: "not-allowed",
                border:
                  "1px solid var(--nv-sys-color-personal-dark-border-button-secondary-disabled)",
              },
            },
            tertiary: {
              standard: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-standard-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-standard-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-standard-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              delete: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-delete-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-delete-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-delete-button-bg)",
                },
                focus: {
                  border: "none",
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              success: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-success-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-success-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-success-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-success-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  border: "none",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              neutral: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-neutral-button-bg)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-neutral-hover)",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-neutral-button-bg)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-personal-dark-background-button-tertiary-neutral-pressed)",
                  border: "none",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-neutral-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  border: "none",
                  outlineColor:
                    "var(--nv-sys-color-personal-light-border-general-focus)",
                },
              },
              disabled: {
                border: "none",
                background: "transparent",
                color:
                  "var(--nv-sys-color-personal-dark-text-icon-on-tertiary-disabled-button-bg)",
                cursor: "not-allowed",
              },
            },
          },
        },
        flow: {
          dark: {
            primary: {
              standard: {
                enabled: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-standard-enabled)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-standard-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                  border: "none",
                },
              },
              delete: {
                enabled: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-delete-enabled)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-delete-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-delete-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-delete-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-delete-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                  border: "none",
                },
              },
              success: {
                enabled: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-success-enabled)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-success-button-bg)",
                  border: "none",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-success-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-success-button-bg)",
                  border: "none",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-primary-success-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-primary-success-button-bg)",
                  border: "none",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                  border: "none",
                },
              },
              disabled: {
                background:
                  "var(--nv-sys-color-flow-dark-background-button-primary-standard-disabled)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-primary-disabled-button-bg)",
                cursor: "not-allowed",
                border: "none",
              },
            },
            secondary: {
              standard: {
                enabled: {
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-standard)",
                  background: "transparent",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-standard-hover)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-standard)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-personal-dark-text-icon-on-secondary-standard-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-standard)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                  background: "none",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-standard)",
                },
              },
              delete: {
                enabled: {
                  background: "transparent",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-delete)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-delete-button-bg)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-delete-hover)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-delete-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-delete)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-delete-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-delete)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-delete)",
                },
              },
              success: {
                enabled: {
                  background: "transparent",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-success-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-success)",
                },
                hover: {
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-success)",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-success-hover)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-success-button-bg)",
                },
                pressed: {
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-success)",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-success-pressed)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-success-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                },
              },
              neutral: {
                enabled: {
                  background: "transparent",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-neutral)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-neutral-hover)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-neutral)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-secondary-neutral-pressed)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-secondary-neutral-button-bg)",
                  border:
                    "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-neutral)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-light-border-general-focus)",
                },
              },
              disabled: {
                background:
                  "var(--nv-sys-color-flow-dark-background-button-primary-standard-disabled)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-secondary-disabled-button-bg)",
                cursor: "not-allowed",
                border:
                  "1px solid var(--nv-sys-color-flow-dark-border-button-secondary-disabled)",
              },
            },
            tertiary: {
              standard: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-standard-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-standard-hover)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-standard-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-standard-pressed)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-standard-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                },
              },
              delete: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-delete-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-delete-hover)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-delete-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-delete-pressed)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-delete-button-bg)",
                },
                focus: {
                  border: "none",
                  outlineOffset: "4px",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                },
              },
              success: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-success-button-bg)",
                },
                hover: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-success-hover)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-success-button-bg)",
                },
                pressed: {
                  border: "none",
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-success-pressed)",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-success-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  border: "none",
                  outlineColor:
                    "var(--nv-sys-color-flow-dark-border-general-focus)",
                },
              },
              neutral: {
                enabled: {
                  background: "transparent",
                  border: "none",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-neutral-button-bg)",
                },
                hover: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-neutral-hover)",
                  border: "none",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-neutral-button-bg)",
                },
                pressed: {
                  background:
                    "var(--nv-sys-color-flow-dark-background-button-tertiary-neutral-pressed)",
                  border: "none",
                  color:
                    "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-neutral-button-bg)",
                },
                focus: {
                  outlineOffset: "4px",
                  border: "none",
                  outlineColor:
                    "var(--nv-sys-color-flow-light-border-general-focus)",
                },
              },
              disabled: {
                border: "none",
                background:
                  "var(--nv-sys-color-flow-dark-background-button-primary-standard-disabled)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tertiary-disabled-button-bg)",
              },
            },
          },
        },
      },
    };
  }
  public getValues() {
    return this.MATRIX;
  }
  public getMeasurements() {
    return this.MATRIX.measures;
  }
  public getColors(): any {
    return this.MATRIX.colors;
  }
  public getAttributesDisabled(
    brand: BrandType,
    theme: ThemeType,
    variant: VariantType,
    attribute: AttributeType,
  ): any {
    try {
      const realValue =
        this.MATRIX.colors[brand][theme]?.[variant]?.disabled?.[attribute];
      if (realValue) {
        return realValue as any;
      } else {
        if (brand === BRANDPERSONAL) {
          return "var(--nv-sys-color-personal-light-background-button-primary-standard-enabled)" as any;
        }
      }
    } catch (e) {
      console.error(`Error en Button Atributo Disabled: ${e}`);
    }
    return "var(--nv-sys-color-flow-dark-background-button-primary-standard-enabled)" as any;
  }

  public getAttribute(
    brand: BrandType,
    theme: ThemeType,
    variant: VariantType,
    subvariant: SubvariantType,
    action: ActionType,
    attribute: AttributeType,
  ): any {
    try {
      const realValue =
        this.MATRIX.colors[brand][theme]?.[variant]?.[subvariant]?.[action]?.[
          attribute
        ];
      if (realValue) return realValue as any;
    } catch (e) {
      console.error(`Error en Button Atributo Disabled: ${e}`);
    }
    return brand === BRANDPERSONAL
      ? ("var(--nv-sys-color-personal-light-background-button-primary-standard-enabled)" as any)
      : ("var(--nv-sys-color-flow-dark-background-button-primary-standard-enabled)" as any);
  }

  public getAttributeMeasurements(device: DeviceType, size: SizeDeviceType) {
    return this.getMeasurements()[device][size];
  }
}
