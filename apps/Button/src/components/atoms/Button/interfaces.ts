import { PixelNumberType } from "../../../types/atoms/types";
import {
  BrandType,
  ThemeType,
  VariantType,
  SubvariantType,
  SizeDeviceType,
  ButtonPropsTokenColorType,
} from "./types";

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  tokenColor?: ButtonPropsTokenColorType;
  size?: SizeDeviceType;
  children?: React.ReactNode;
}

export interface ButtonSCProps {
  $brand: BrandType;
  $theme: ThemeType;
  $variant: VariantType;
  $subvariant: SubvariantType;
  children: React.ReactNode;
}
interface ButtonClassColorsHoverPropertiesType {
  background?: string;
  color?: string;
  border?: string;
  outlineOffset?: PixelNumberType;
  outlineColor?: string;
  cursor?: string;
}
interface ButtonClassColorsActionsType {
  hover: ButtonClassColorsHoverPropertiesType;
  focus: ButtonClassColorsHoverPropertiesType;
  pressed: ButtonClassColorsHoverPropertiesType;
  enabled: ButtonClassColorsHoverPropertiesType;
}
interface ButtonClassColorsPropertiesType {
  standard: ButtonClassColorsActionsType;
  delete: ButtonClassColorsActionsType;
  success: ButtonClassColorsActionsType;
  neutral?: ButtonClassColorsActionsType;
  disabled: ButtonClassColorsHoverPropertiesType;
}
interface ButtonClassColorsVariantsTyps {
  primary: ButtonClassColorsPropertiesType;
  secondary: ButtonClassColorsPropertiesType;
  tertiary: ButtonClassColorsPropertiesType;
}
interface ButtonClassColorsThemeTyps {
  light?: ButtonClassColorsVariantsTyps;
  dark?: ButtonClassColorsVariantsTyps;
}
interface ButtonClassColorsType {
  personal: ButtonClassColorsThemeTyps;
  flow: ButtonClassColorsThemeTyps;
}

export interface ButtonClassMeasuresMatrixType {
  measures: ButtonClassMeasuresDeviceType;
  colors: ButtonClassColorsType;
}

export interface ButtonClassMeasuresDeviceType {
  desktop: ButtonClassMeasuresPropertiesType;
  mobile: ButtonClassMeasuresPropertiesType;
}
interface ButtonClassMeasuresPropertiesType {
  small: ButtonClassMeasuresAttributesInterface;
  medium: ButtonClassMeasuresAttributesInterface;
  large: ButtonClassMeasuresAttributesInterface;
}
export interface ButtonClassMeasuresAttributesInterface {
  height: PixelNumberType;
  widthWidthOutIcon: PixelNumberType;
  fontSize: PixelNumberType;
  charactersCount: ButtonClassMeasuresAttributesCharactersCountInterface;
  iconPosition: "left" | "right";
  iconAlign:
    | "center"
    | "top"
    | "bottom"
    | "left"
    | "right"
    | "top-left"
    | "top-right"
    | "bottom-left"
    | "bottom-right";
  iconSize: PixelNumberType;
  borderRadius?: PixelNumberType;
  borderRadiusFocus?: PixelNumberType;
  marginLeft: PixelNumberType;
  marginRight: PixelNumberType;
  marginTop: PixelNumberType;
  marginBottom: PixelNumberType;
  gap: PixelNumberType;
}

export interface ButtonClassMeasuresAttributesCharactersCountInterface {
  linesByText: number;
  min: number;
  max: number;
}

const VALID_MIN_REGEX = /^[5-9]|[1-9][0-9]+$/;
const charactersCount: ButtonClassMeasuresAttributesCharactersCountInterface = {
  linesByText: 1,
  min: 5,
  max: 20,
};

Object.defineProperty(charactersCount, "min", {
  get: function () {
    return this._min;
  },
  set: function (value: number) {
    if (typeof value !== "number" || !VALID_MIN_REGEX.test(value.toString())) {
      throw new Error("El valor mínimo debe ser un número mayor o igual a 5.");
    }
    this._min = value;
  },
  enumerable: true,
  configurable: true,
});
