// import { ValidColorsType } from "@core/ds-icons/dist/ValidValuesType";

export type DeviceType = "mobile" | "desktop";
export type SizeDeviceType = "small" | "medium" | "large";

export type PixelNumberType = `${number}px`;
export type PorcentualNumberType = `${number}%`;
export type NoneType = "none";
export type OutlineOffsetType = `${PixelNumberType} | ${NoneType}`;
export type BorderStyleType =
  | "solid"
  | "dotted"
  | "dashed"
  | "double"
  | "groove"
  | "ridge"
  | "inset"
  | "outset"
  | "none";
export type BorderType =
  `${PixelNumberType} ${BorderStyleType} ${any}`;
export type MeasurentsMapType =
  | "height"
  | "widthWidthOutIcon"
  | "fontSize"
  | "borderRadius"
  | "marginLeft"
  | "marginRight"
  | "marginTop"
  | "marginBottom"
  | "gap";
export type BrandType = "personal" | "flow";
export type ThemeType = "light" | "dark";
export type VariantType = "primary" | "secondary" | "tertiary";
export type SubvariantType =
  | "standard"
  | "success"
  | "delete"
  | "neutral"
  | "disabled";
export type ActionType = "enabled" | "pressed" | "hover" | "focus";
export type AttributeType =
  | "background"
  | "color"
  | "border"
  | "outlineOffset"
  | "outlineColor";
export type ButtonPropsTokenColorType =
  | "primary-standard"
  | "primary-success"
  | "primary-delete"
  | "secondary-standard"
  | "secondary-success"
  | "secondary-delete"
  | "secondary-neutral"
  | "tertiary-standard"
  | "tertiary-success"
  | "tertiary-delete"
  | "tertiary-neutral";
