/**
 * This file contains the styled components for the Button component.
 */

import { ClassButton } from "./classes";
import styled, { css } from "styled-components";
import {
  BrandType,
  DeviceType,
  SizeDeviceType,
  SubvariantType,
  ThemeType,
  ButtonPropsTokenColorType,
  VariantType,
} from "./types";

const buttonObject = new ClassButton();

/**
 * StyledButton component represents the styled button element.
 *
 * @remarks
 * It accepts the following props:
 * - $brand: The brand type of the button.
 * - $theme: The theme type of the button.
 * - $tokenColor: The token color type of the button.
 * - $device: The device type of the button.
 * - $size: The size device type of the button.
 */
export const StyledButton = styled.button<{
  $brand: BrandType;
  $theme: ThemeType;
  $tokenColor: ButtonPropsTokenColorType;
  $device: DeviceType;
  $size: SizeDeviceType;
}>`
  ${(props) => {
    const [variant, subvariant] = props.$tokenColor.split("-") as [
      VariantType,
      SubvariantType,
    ];
    const { $brand, $theme, $device, $size } = props;
    return css`
      background-color: ${buttonObject.getAttribute(
        $brand,
        $theme,
        variant,
        subvariant,
        "enabled",
        "background",
      )};
      color: ${buttonObject.getAttribute(
        $brand,
        $theme,
        variant,
        subvariant,
        "enabled",
        "color",
      )};
      border: ${buttonObject.getAttribute(
        $brand,
        $theme,
        variant,
        subvariant,
        "enabled",
        "border",
      )};
      height: ${buttonObject.getAttributeMeasurements($device, $size).height};
      font-size: ${buttonObject.getAttributeMeasurements($device, $size)
        .fontSize};
      border-radius: ${buttonObject.getAttributeMeasurements($device, $size)
        .borderRadius};
      padding-left: ${buttonObject.getAttributeMeasurements($device, $size)
        .marginLeft};
      padding-right: ${buttonObject.getAttributeMeasurements($device, $size)
        .marginRight};
      padding-top: ${buttonObject.getAttributeMeasurements($device, $size)
        .marginTop};
      padding-bottom: ${buttonObject.getAttributeMeasurements($device, $size)
        .marginBottom};
      min-width: ${buttonObject.getAttributeMeasurements($device, $size)
        .widthWidthOutIcon};
      width: fit-content;
      &:hover {
        background-color: ${buttonObject.getAttribute(
          $brand,
          $theme,
          variant,
          subvariant,
          "hover",
          "background",
        )};
        border: ${buttonObject.getAttribute(
          $brand,
          $theme,
          variant,
          subvariant,
          "hover",
          "border",
        )};
      }
      &:active {
        background-color: ${buttonObject.getAttribute(
          $brand,
          $theme,
          variant,
          subvariant,
          "pressed",
          "background",
        )};
        border: ${buttonObject.getAttribute(
          $brand,
          $theme,
          variant,
          subvariant,
          "enabled",
          "border",
        )};
      }
      &:disabled {
        background-color: ${buttonObject.getAttributesDisabled(
          $brand,
          $theme,
          variant,
          "background",
        )};
        color: ${buttonObject.getAttributesDisabled(
          $brand,
          $theme,
          variant,
          "color",
        )};
        border: ${buttonObject.getAttributesDisabled(
          $brand,
          $theme,
          variant,
          "border",
        )};
      }
      &:focus {
        outline-offset: 4px;
        outline-color: var(
          --nv-sys-color-${$brand}-${$theme}-border-general-focus
        );
      }
    `;
  }}
`;

/**
 * StyledWrapper component represents the styled wrapper div for the Button component.
 *
 * @remarks
 * It accepts the following props:
 * - $device: The device type of the button.
 * - $size: The size device type of the button.
 */
export const StyledWrapper = styled.div<{
  $device: DeviceType;
  $size: SizeDeviceType;
}>`
  ${(props) => {
    const { $device, $size } = props;
    return css`
      display: flex;
      justify-content: center;
      align-items: center;
      gap: ${buttonObject.getAttributeMeasurements($device, $size).gap};
    `;
  }}
`;
