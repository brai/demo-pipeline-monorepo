import { useState } from "react";
import { BrandType, ThemeType } from "../types/atoms/types";
import { BRANDPERSONAL, THEMEDARK, THEMELIGHT } from "../constants";

export interface UseInitialBrandThemeProps {
  initialBrand: BrandType;
  initialTheme?: ThemeType;
}

export const useInitialBrandTheme = ({
  initialBrand,
  initialTheme,
}: UseInitialBrandThemeProps) => {
  const [brand, setBrand] = useState<BrandType>(initialBrand);
  const [theme, setTheme] = useState<ThemeType>(
    initialTheme || (initialBrand === BRANDPERSONAL ? THEMELIGHT : THEMEDARK),
  );
  return { brand, theme, setBrand, setTheme };
};
