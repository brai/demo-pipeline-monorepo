import React, { useState } from "react";
import styled from "styled-components";

function App() {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(!open);
  };

  const [selectedValue, setSelectedValue] = useState("");

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedValue(event.target.value);
  };

  const [isChecked, setIsChecked] = useState(false);
  return (
    <>
    </>
  );
}

const Wrapper = styled.div`
  margin: 2rem;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 8px 8px;
`;

export default App;
