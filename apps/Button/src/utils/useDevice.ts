import { useState, useEffect } from "react";
import { DeviceType } from "../components/atoms/Button/types";

const useDevice = () => {
  const [sizeScreen, setSizeScreen] = useState<DeviceType>("desktop");

  useEffect(() => {
    const handleResize = () => {
      const isDesktop = window.innerWidth >= 600;
      setSizeScreen(isDesktop ? "desktop" : "mobile");
    };
    window.addEventListener("resize", handleResize);
    handleResize();
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return { sizeScreen };
};

export default useDevice;
