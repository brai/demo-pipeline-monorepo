import styled from "styled-components";

/**
 * The type of display for the Flex component.
 */
export type FlexDisplayType = "flex" | "inline-flex";

/**
 * The type of flex direction for the Flex component.
 */
export type FlexDirectionType =
  | "row"
  | "row-reverse"
  | "column"
  | "column-reverse";

/**
 * The type of justify content for the Flex component.
 */
export type JustifyContentType =
  | "flex-start"
  | "flex-end"
  | "center"
  | "space-between"
  | "space-around"
  | "space-evenly";

/**
 * The type of align items for the Flex component.
 */
export type AlignItemsType =
  | "stretch"
  | "flex-start"
  | "flex-end"
  | "center"
  | "baseline";

/**
 * The type of flex wrap for the Flex component.
 */
export type FlexWrapType = "nowrap" | "wrap" | "wrap-reverse";

/**
 * Props for the Flex component.
 */
interface FlexProps {
  display?: FlexDisplayType;
  flexDirection?: FlexDirectionType;
  justifyContent?: JustifyContentType;
  alignItems?: AlignItemsType;
  flexWrap?: FlexWrapType;
  children?: React.ReactNode;
  gap?: string;
}

/**
 * Styled Flex component.
 */
const StyledFlex = styled.div<FlexProps>`
  display: ${({ display }) => display || "flex"};
  flex-direction: ${({ flexDirection }) => flexDirection || "row"};
  justify-content: ${({ justifyContent }) => justifyContent || "flex-start"};
  align-items: ${({ alignItems }) => alignItems || "stretch"};
  flex-wrap: ${({ flexWrap }) => flexWrap || "nowrap"};
  gap: ${({ gap }) => gap || "0"};
  padding: 0;
  margin: 0;
`;

/**
 * Flex component.
 */
const Flex = ({
  display,
  flexDirection,
  justifyContent,
  alignItems,
  flexWrap,
  children,
  gap,
}: FlexProps) => {
  return (
    <StyledFlex
      display={display}
      flexDirection={flexDirection}
      justifyContent={justifyContent}
      alignItems={alignItems}
      flexWrap={flexWrap}
      gap={gap}
    >
      {children}
    </StyledFlex>
  );
};

export default Flex;
