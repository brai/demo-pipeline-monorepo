

## [0.7.0](https://gitlab.com/brai/demo-pipeline-monorepo/compare/@mono/app-b-v0.6.0...@mono/app-b-v0.7.0) (2024-04-27)


### Features

* **config-release-it:** released version v0.4.0 [no ci] ([12f45ce](https://gitlab.com/brai/demo-pipeline-monorepo/commit/12f45cef8aa34e74362a9b0c9a78c277a7817f60))
* **lib-b:** released version v0.6.0 [no ci] ([3bf0375](https://gitlab.com/brai/demo-pipeline-monorepo/commit/3bf03757092a22919c680e38c36996bcee2c1ae7))
* publishConfig ([3675a4f](https://gitlab.com/brai/demo-pipeline-monorepo/commit/3675a4f63633c251ed30d11f10926dd891739851))

## [0.6.0](https://gitlab.com/brai/demo-pipeline-monorepo/compare/@mono/app-b-v0.5.0...@mono/app-b-v0.6.0) (2024-04-27)


### Features

* **config-release-it:** released version v0.3.0 [no ci] ([7b70ef1](https://gitlab.com/brai/demo-pipeline-monorepo/commit/7b70ef1bdd6cc928e1865c3bd3caed95662e7311))
* **lib-b:** released version v0.5.0 [no ci] ([abf9bb2](https://gitlab.com/brai/demo-pipeline-monorepo/commit/abf9bb23143a119dd47f38781ccfb1a706e1c799))

## 0.5.0 (2024-04-27)


### Features

* **config-release-it:** released version v0.2.0 [no ci] ([cedae74](https://gitlab.com/brai/demo-pipeline-monorepo/commit/cedae7425c30e79b0b869c229b7baf37bff1d81a))
* **lib-b:** released version v0.4.0 [no ci] ([d93787b](https://gitlab.com/brai/demo-pipeline-monorepo/commit/d93787b5c8df7819f52017a0d978bdc81aa51e0f))
* prueba desplieque paquetes con monorepo ([182469a](https://gitlab.com/brai/demo-pipeline-monorepo/commit/182469a8c416eeed0a633b7e384e44d556684e95))

## [0.4.0](https://github.com/b12k/monorepo-semantic-releases/compare/@mono/app-b-v0.3.0...@mono/app-b-v0.4.0) (2023-05-01)


### Features

* **lib-b:** added prefix ([#3](https://github.com/b12k/monorepo-semantic-releases/issues/3)) ([2cb0847](https://github.com/b12k/monorepo-semantic-releases/commit/2cb08478f16b3efa133c5af2b632c14f295ac2ff))

## [0.3.0](https://github.com/b12k/monorepo-semantic-releases/compare/@mono/app-b-v0.2.0...@mono/app-b-v0.3.0) (2023-05-01)


### Features

* **lib-b:** released version v0.3.0 [no ci] ([54a39cd](https://github.com/b12k/monorepo-semantic-releases/commit/54a39cd3309e052d8e4682d3e0c31e06ac890674))

## [0.2.0](https://github.com/b12k/monorepo-semantic-releases/compare/@mono/app-b-v0.1.0...@mono/app-b-v0.2.0) (2023-05-01)


### Features

* **lib-b:** released version v0.2.0 [no ci] ([73f5876](https://github.com/b12k/monorepo-semantic-releases/commit/73f587631a469011022e53599b9ebb864ea4a7c7))

## 0.1.0 (2023-05-01)


### Features

* **config-release-it:** released version v0.1.0 [no ci] ([f0446fc](https://github.com/b12k/monorepo-semantic-releases/commit/f0446fc59c62a71c8d9847d38f6de84f001540ad))
* **lib-b:** released version v0.1.0 [no ci] ([62eccd5](https://github.com/b12k/monorepo-semantic-releases/commit/62eccd51c89c12413e352a0fcaee68aefd0401bf))
* **repo:** initial commit ([5849273](https://github.com/b12k/monorepo-semantic-releases/commit/58492737f01fe3a2fd98e0b2b3c0646e6850a8db))