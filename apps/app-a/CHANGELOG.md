

## [0.5.0](https://gitlab.com/brai/demo-pipeline-monorepo/compare/@mono/app-a-v0.4.0...@mono/app-a-v0.5.0) (2024-04-27)


### Features

* **config-release-it:** released version v0.4.0 [no ci] ([12f45ce](https://gitlab.com/brai/demo-pipeline-monorepo/commit/12f45cef8aa34e74362a9b0c9a78c277a7817f60))
* **lib-a:** released version v0.5.0 [no ci] ([04e3ff4](https://gitlab.com/brai/demo-pipeline-monorepo/commit/04e3ff4d1b07b52e2f4ae4e4498a89699d73afea))

## [0.4.0](https://gitlab.com/brai/demo-pipeline-monorepo/compare/@mono/app-a-v0.3.0...@mono/app-a-v0.4.0) (2024-04-27)


### Features

* **config-release-it:** released version v0.3.0 [no ci] ([7b70ef1](https://gitlab.com/brai/demo-pipeline-monorepo/commit/7b70ef1bdd6cc928e1865c3bd3caed95662e7311))
* **lib-a:** released version v0.4.0 [no ci] ([c062240](https://gitlab.com/brai/demo-pipeline-monorepo/commit/c062240e086d5f2247b9757d4ba2fb3a5d66a6f5))

## 0.3.0 (2024-04-27)


### Features

* **config-release-it:** released version v0.2.0 [no ci] ([cedae74](https://gitlab.com/brai/demo-pipeline-monorepo/commit/cedae7425c30e79b0b869c229b7baf37bff1d81a))
* **lib-a:** released version v0.3.0 [no ci] ([dea4f32](https://gitlab.com/brai/demo-pipeline-monorepo/commit/dea4f320dc8b888c947bdd8c865654d8d66e2680))
* prueba desplieque paquetes con monorepo ([182469a](https://gitlab.com/brai/demo-pipeline-monorepo/commit/182469a8c416eeed0a633b7e384e44d556684e95))
* update package.json ([4c79abf](https://gitlab.com/brai/demo-pipeline-monorepo/commit/4c79abfd678415b815c9ac9e17591451cdb30ec1))

## [0.2.1](https://github.com/b12k/monorepo-semantic-releases/compare/@mono/app-a-v0.2.0...@mono/app-a-v0.2.1) (2023-05-01)


### Bug Fixes

* **lib-a:** added prefix ([#4](https://github.com/b12k/monorepo-semantic-releases/issues/4)) ([9c77d35](https://github.com/b12k/monorepo-semantic-releases/commit/9c77d3553e3c08442f210d4dd337737fee6907d2))

## [0.2.0](https://github.com/b12k/monorepo-semantic-releases/compare/@mono/app-a-v0.1.0...@mono/app-a-v0.2.0) (2023-05-01)


### Features

* **lib-a:** released version v0.2.0 [no ci] ([a5a135a](https://github.com/b12k/monorepo-semantic-releases/commit/a5a135a0f5e94593402c29788fe683c76f3c7c86))

## 0.1.0 (2023-05-01)


### Features

* **config-release-it:** released version v0.1.0 [no ci] ([f0446fc](https://github.com/b12k/monorepo-semantic-releases/commit/f0446fc59c62a71c8d9847d38f6de84f001540ad))
* **lib-a:** released version v0.1.0 [no ci] ([7e9365d](https://github.com/b12k/monorepo-semantic-releases/commit/7e9365d3f642fcbcbb415a6bafdd2711d6084d4d))
* **repo:** initial commit ([5849273](https://github.com/b12k/monorepo-semantic-releases/commit/58492737f01fe3a2fd98e0b2b3c0646e6850a8db))