import styled from "styled-components";

/**
 * Props for the Container component.
 */
interface ContainerProps {
  pleft?: string;
  pright?: string;
  ptop?: string;
  pbottom?: string;
  px?: string;
  py?: string;
  pall?: string;
  mleft?: string;
  mright?: string;
  mtop?: string;
  mbottom?: string;
  mx?: string;
  my?: string;
  mall?: string;
  gap?: string;
  children: React.ReactNode;
}

/**
 * Styled container component with customizable padding and margin.
 *
 * @component
 * @example
 * ```tsx
 * <StyledContainer pleft="10px" pright="20px" ptop="5px" pbottom="15px" />
 * ```
 */
const StyledContainer = styled.div<ContainerProps>`
  padding-left: ${({ pleft, px, pall }) => pleft || px || pall || "0"};
  padding-right: ${({ pright, px, pall }) => pright || px || pall || "0"};
  padding-top: ${({ ptop, py, pall }) => ptop || py || pall || "0"};
  padding-bottom: ${({ pbottom, py, pall }) => pbottom || py || pall || "0"};

  margin-left: ${({ mleft, mx, mall }) => mleft || mx || mall || "0"};
  margin-right: ${({ mright, mx, mall }) => mright || mx || mall || "0"};
  margin-top: ${({ mtop, my, mall }) => mtop || my || mall || "0"};
  margin-bottom: ${({ mbottom, my, mall }) => mbottom || my || mall || "0"};

  gap: ${({ gap }) => gap || "0"};
`;

/**
 * A container component that wraps its children with a styled container.
 *
 * @component
 * @example
 * ```tsx
 * <Container>
 *   <div>Content goes here</div>
 * </Container>
 * ```
 */
const Container: React.FC<ContainerProps> = ({ children, ...props }) => (
  <StyledContainer {...props}>{children}</StyledContainer>
);

export default Container;
