import { createContext } from "react";
import { BrandContextType } from "../types/atoms/types";
import { BrandProviderPropsInterface } from "../types/atoms/interfaces";
import {
  BRANDPERSONAL,
  BRANDCONTEXTCOMPONENTS,
  THEMELIGHT,
} from "../constants";
import { useInitialBrandTheme } from "../hooks/useInitialBrandTheme";

export const BrandContext = createContext<BrandContextType>({
  brand: BRANDPERSONAL,
  theme: THEMELIGHT,
  setBrand: () => null,
  setTheme: () => null,
  componentThemes: BRANDCONTEXTCOMPONENTS,
});

export const BrandProvider = ({
  children,
  brand: initialBrand = BRANDPERSONAL,
  theme: initialTheme,
}: BrandProviderPropsInterface) => {
  const { brand, theme, setBrand, setTheme } = useInitialBrandTheme({
    initialBrand,
    initialTheme,
  });

  return (
    <BrandContext.Provider
      value={{
        brand,
        theme,
        setBrand,
        setTheme,
        componentThemes: BRANDCONTEXTCOMPONENTS,
      }}
    >
      {children}
    </BrandContext.Provider>
  );
};
