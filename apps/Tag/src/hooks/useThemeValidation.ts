import { useEffect } from "react";
import { useBrand } from "./useBrand";
import { BRANDPERSONAL } from "../constants";

const useThemeValidation = (componentName: string) => {
  const { brand, theme, setTheme, componentThemes } = useBrand();

  useEffect(() => {
    const componentConfig = componentThemes.find(
      (config) => config.component === componentName,
    );

    if (!componentConfig) {
      throw new Error(`${componentName} configuration not found`);
    }

    const acceptedThemes =
      brand === BRANDPERSONAL
        ? componentConfig.themesPersonal
        : componentConfig.themesFlow;
    if (!acceptedThemes.includes(theme)) {
      console.log(
        `El componente ${componentName} no acepta al Theme ${theme}. Se setea ${acceptedThemes[0]}`,
      );
      setTheme(acceptedThemes[0]);
    }
  }, [brand, theme, setTheme, componentName, componentThemes]);

  return { theme };
};

export default useThemeValidation;
