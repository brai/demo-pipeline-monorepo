import { useContext } from "react";
import { BrandContext } from "../contexts/BrandContext";

export const useBrand = () => {
  const context = useContext(BrandContext);
  if (!context) {
    throw new Error("useBrand debe ser usado con BrandProvider");
  }
  return context;
};
