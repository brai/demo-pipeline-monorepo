import { css, styled } from "styled-components";
import {  TagClass } from "./classes"
import { BrandType, DeviceType, SubvariantType, ThemeType, VariantType } from "./types";
import { TokenColorType } from "./types.js";

const tag = new TagClass();

export const TagSC = styled.div<{ $brand: BrandType, $theme: ThemeType ,$token: TokenColorType, $device: DeviceType}>`
    display: flex;
    align-items: center;
    justify-content: center;
    border: none;
    ${props => {
        const {$brand, $theme, $token, $device} = props
        const [variant, subVariant] = $token.split('-') as [VariantType, SubvariantType]
        console.log(props)
        return(css`
            font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen','Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',sans-serif;
            height: ${tag.getAttributeMeasurements($device).height};
            width: ${tag.getAttributeMeasurements($device).maxWidth};
            margin: ${tag.getAttributeMeasurements($device).margin};
            border-radius: ${tag.getAttributeMeasurements($device).borderRadius};
            background-color: ${tag.getColors($brand,$theme, variant, subVariant, 'background')};
            color: ${tag.getColors($brand, $theme, variant, subVariant, 'color')};
            font-size: ${tag.getAttributeMeasurements($device).fontSize};
            font-size: ${tag.getAttributeMeasurements($device).iconSize};
            gap: ${tag.getAttributeMeasurements($device).gap};
            text-align: ${tag.getAttributeMeasurements($device).align};
            margin: ${tag.getAttributeMeasurements($device).margin};
            fill: ${tag.getColors($brand, $theme, variant,subVariant, 'color')};

            `
        )
    }}
`
