import {
  AttributeType,
  BrandType,
  DeviceType,
  PixelNumberType,
  SubvariantType,
  ThemeType,
  VariantType,
} from "./types";
import { AttributesMeasuresInterface } from "./interfaces";
import { BRANDPERSONAL } from "../../../constants";


type DeviceObjectType = {
  height:PixelNumberType,
  margin: PixelNumberType,
  borderRadius: string,
  padding: string,
  background?: string,
  flexShrink?: number,
  Icon?: PixelNumberType,
  outline?: string,
  maxWidth: PixelNumberType,
  fontSize: PixelNumberType,
  gap: string,
  align: string
}
type MeasuresConstants = {
  mobile: DeviceObjectType;
  desktop: DeviceObjectType;
}
type PropertiesTagType = {
  background: string;
  color: string;

}
type TagVariantColors = {
  primary: {
      soft: PropertiesTagType;
      bold: PropertiesTagType;
  };
  secondary: {
    soft: PropertiesTagType;
    bold: PropertiesTagType;
};
tertiary: {
  soft: PropertiesTagType;
  bold: PropertiesTagType;
};
warning: {
  soft: PropertiesTagType;
  bold: PropertiesTagType;
};
success: {
  soft: PropertiesTagType;
  bold: PropertiesTagType;
};
error: {
  soft: PropertiesTagType;
  bold: PropertiesTagType;
};
gray: {
  soft: PropertiesTagType;
  bold: PropertiesTagType;
};

}
type BrandThemes = {
  light?: TagVariantColors;
  dark?: TagVariantColors;
};
type BrandConstants = {
  personal: BrandThemes;
  flow: BrandThemes;
};
type TagConstants = {
  colors: BrandConstants;
  measures: MeasuresConstants;

}
export class TagClass {
  public MATRIX: TagConstants;

  constructor() {
    this.MATRIX = {
      measures: {
        desktop: {
          height: '100px',
          maxWidth:'700px',
          fontSize: "12px",
          Icon: "16px",
          borderRadius: "20px",
          padding:'2px 2px',
          gap:'var(--nv-sys-spacing-2xs)',
          align: 'center',
          margin: '4px',
        },
        mobile: {
          height: '20px',
          maxWidth:'60px',
          fontSize: "12px",
          Icon: "16px",
          borderRadius: "20px",
          padding:'2px 2px',
          gap:'var(--nv-sys-spacing-2xs)',
          align: 'center',
          margin: '4px',
        },
      },
      colors: {
        personal: {
          light: {
            primary: {
              soft: {
                background:
                  "var(--nv-sys-color-personal-light-background-tag-primary-soft)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-primary)",
              },
              bold: {
                background:
                  "var(--nv-sys-color-personal-light-background-tag-primary-bold)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-primary)",
              },
            },
            secondary: {
              soft: {
                background:
                  "var(--nv-sys-color-personal-light-background-tag-secondary-soft)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-secondary-soft)",
              },
              bold: {
                background:
                  "var(--nv-sys-color-personal-light-background-tag-secondary-bold)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-general-bold)",
              },
            },
            tertiary: {
              soft: {
                background:
                  "var(--nv-sys-color-personal-light-background-tag-tertiary-soft)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-tertiary-soft)",
              },
              bold: {
                background:
                  "var(--nv-sys-color-personal-light-background-tag-tertiary-bold)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-general-bold)",
              },
            },
            warning: {
              soft: {
                background:
                  "var(--nv-ref-palette-yellow-95)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-warning-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-yellow-80)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-warning-bold)",
              },
            },
            success: {
              soft: {
                background:
                  "var(--nv-ref-palette-green-99)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-success-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-green-40)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-general-bold)",
              },
            },
            error: {
              soft: {
                background:
                  "var(--nv-ref-palette-red-90)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-error-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-red-40)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-general-bold)",
              },
            },
            gray: {
              soft: {
                background:
                  "var(--nv-ref-palette-gray-90)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-gray-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-gray-40)",
                color:
                  "var(--nv-sys-color-personal-light-text-icon-on-tag-bg-general-bold)",
              },
            },
          },
        },
        flow: {
          dark: {
            primary: {
              soft: {
                background:
                  "var(--nv-sys-color-flow-dark-background-tag-primary-soft)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-primary)",
              },
              bold: {
                background:
                  "var(--nv-sys-color-flow-dark-background-tag-primary-bold)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-primary)",
              },
            },
            secondary: {
              soft: {
                background:
                  "var(--nv-sys-color-flow-dark-background-tag-secondary-soft)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-secondary-soft)",
              },
              bold: {
                background:
                  "var(--nv-sys-color-flow-dark-background-tag-secondary-bold)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-general-bold)",
              },
            },
            tertiary: {
              soft: {
                background:
                  "var(--nv-sys-color-flow-dark-background-tag-tertiary-soft)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-tertiary-soft)",
              },
              bold: {
                background:
                  "var(--nv-sys-color-flow-dark-background-tag-tertiary-bold)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-general-bold)",
              },
            },
            warning: {
              soft: {
                background:
                  "var(--nv-ref-palette-yellow-95)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-warning-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-yellow-80)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-warning-bold)",
              },
            },
            success: {
              soft: {
                background:
                  "var(--nv-ref-palette-green-99)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-success-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-green-40)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-general-bold)",
              },
            },
            error: {
              soft: {
                background:
                  "var(--nv-ref-palette-red-90)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-error-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-red-40)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-general-bold)",
              },
            },
            gray: {
              soft: {
                background:
                  "var(--nv-ref-palette-gray-90)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-gray-soft)",
              },
              bold: {
                background:
                  "var(--nv-ref-palette-gray-40)",
                color:
                  "var(--nv-sys-color-flow-dark-text-icon-on-tag-bg-general-bold)",
              },
            },
          },
        },
      },
    };
  }
  public getValues() {
    return this.MATRIX;
  }
  public getMeasures() {
    return this.MATRIX.measures;
  }


  public getAttributeMeasurements(
    device: DeviceType
  ): any {
    return this.getMeasures()[device];
  }

  getColors(brand: BrandType, theme: ThemeType | string, variant: VariantType| string, subvariant: SubvariantType, attribute: AttributeType ) {
    let realValue;
    try{
        realValue = this.getValues().colors[brand][theme][variant][subvariant][attribute]
    }
    catch(e){
        console.error(`Error in Class.getColors: ${e}`)
    }
    return realValue !== undefined ? realValue : brand === BRANDPERSONAL ? "var(--nv-sys-color-personal-light-background-tag-primary-soft)" : "var(--nv-sys-color-flow-dark-background-tag-primary-soft)";
}
}
