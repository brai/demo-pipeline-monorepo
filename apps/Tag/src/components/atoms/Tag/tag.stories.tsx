import { Meta, StoryObj } from '@storybook/react';
import Tag from './index';
import useThemeValidation from '../../../hooks/useThemeValidation';
import { useBrand } from '../../../hooks/useBrand';

const withReactMountHook = (Story, context) => {
    const { theme } = useThemeValidation('Tag');
    const {setBrand} = useBrand()
  
    setBrand(context.globals.brand);
  
    return <Story {...context} args={{ ...context.args, theme }} />;
  };
  
  const meta: Meta<typeof Tag> = {
    title: 'DS/Components/Atoms/Tag',
    component: Tag,
    parameters: {
      layout: 'centered',
    },
    decorators: [withReactMountHook],
    tags: ['autodocs'],
  };

export default meta;

type Story = StoryObj<typeof meta>;

export const Primary: Story = {
    args: {
        token:'primary-soft',
        label:'Soft',
        showIcon: true,
        
    },
};
export const Secondary: Story = {
    args: {
        token:'secondary-soft',
        label:'Soft'
        
    },
};
export const Tertiary: Story = {
    args: {
        token:'tertiary-soft',
        label:'Soft'
        
    },
};
export const Warning: Story = {
    args: {
        token:'warning-soft',
        label:'Soft'
        
    },
};
export const Gray: Story = {
    args: {
        token:'gray-soft',
        label:'Soft'
        
    },
};
export const Error: Story = {
    args: {
        token:'error-soft',
        label:'Soft'
        
    },
};
export const Success: Story = {
    args: {
        token:'success-soft',
        label:'Soft'
        
    },  
};