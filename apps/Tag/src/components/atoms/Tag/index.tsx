import { TagProps } from "./interfaces";
import { TagSC } from "./styledComponents";
// import { NvSysIconSystemFeedbackDualFalcircleinfo as Icon } from "@core/ds-icons";
import { useBrand } from "../../../hooks/useBrand";
import { COMPONENTS } from "../../../constants";
import useThemeValidation from "../../../hooks/useThemeValidation";
import useDevice from "../../../utils/useDevice";
import { Button } from "@core/ds-component-button";
import { Divider } from "@core/ds-component-divider";

const Tag: React.FC<TagProps> = ({
  token = "primary-soft",
  label = "Text",
  showIcon = false,
  ...rest
}) => {
  const { brand } = useBrand();
  const { theme } = useThemeValidation(COMPONENTS.ATOMS.TAG);
  const { sizeScreen } = useDevice();
  return (
    <TagSC
      $brand={brand}
      $theme={theme}
      $token={token}
      $device={sizeScreen}
      {...rest}
    >
      {/* {showIcon && <Icon />} */}
      {label}
      <Button tokenColor='primary-success'> Hola</Button>
      <Divider.Brand token='secondary-medium'></Divider.Brand>
    </TagSC>
  );
};

export default Tag;
