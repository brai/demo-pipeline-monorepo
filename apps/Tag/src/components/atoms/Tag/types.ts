export type PixelNumberType = `${number}px`
export type BrandType = 'personal' | 'flow';
export type ThemeType = 'light' | 'dark';
export type VariantType = 'primary' | 'secondary' | 'tertiary' | 'warning' | 'succes' | 'error' | 'gray';
export type SubvariantType = 'soft' | 'bold';
export type ActionType = 'enabled' | 'hover' | 'pressed' | 'focus' | 'disabled';
export type AttributeType = 'background' | 'color' | 'border-color' | 'outline';
export type DeviceType =  "mobile" | "desktop";
export type TokenColorType = 
'primary-soft'|
'primary-bold'|
'secondary-soft'|
'secondary-bold'|
'tertiary-soft'|
'tertiary-bold'|
'error-soft'|
'error-bold'|
'warning-soft'|
'warning-bold'|
'success-soft'|
'gray-soft'|
'gray-bold'|
'primary-soft'|
'primary-bold'|
'secondary-soft'|
'secondary-bold'|
'tertiary-soft'|
'tertiary-bold'|
'error-soft'|
'error-bold'|
'warning-soft'|
'warning-bold'|
'gray-soft'|
'gray-bold';