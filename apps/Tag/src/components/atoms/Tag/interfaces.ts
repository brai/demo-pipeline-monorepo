import {  BrandType, VariantType, AttributeType, ActionType, ThemeType, SubvariantType, TokenColorType, DeviceType, PixelNumberType } from './types';

export interface IconSCProps {
    $color?: VariantType;
}
export interface TagProps  {
    token?: TokenColorType;
    variant?: VariantType;
    brand?: BrandType;
    attribute?: AttributeType;
    theme?: ThemeType;
    subvariant?: SubvariantType
    device?: DeviceType;
    label?: React.ReactNode;
    showIcon?: boolean;
}
export interface AttributesMeasuresInterface {
    height?: PixelNumberType;
    width?: PixelNumberType;
    maxWidth?: PixelNumberType;
    minWidth?: PixelNumberType;
    margin?: string;
    borderRadius?: string,
    padding?: string,
    background?: string,
    color?: string;
    fontSize?: PixelNumberType;
    iconSize?: PixelNumberType;
    gap?: PixelNumberType;
    align?: string;
}

export interface TagSCProps {
    $variant?: VariantType;
    $brand?: BrandType;
    $action?: ActionType;
    $theme?: ThemeType ;
}

/**********************************/
export interface ColorStyle {
    background: String;
    color: String;
    border?: string; // Este campo es opcional para algunos casos
}

interface ColorState {
    default: ColorStyle;
    pressed?: ColorStyle;
    hover?: ColorStyle;
    focus?: ColorStyle;
}

interface ColorGroup {
    default: ColorStyle;
    pressed?: ColorStyle;
    hover?: ColorStyle;
    focus?: ColorStyle;
}

interface ColorType {
    primary: ColorState;
    gray: ColorState;
    disabled?: ColorGroup;
}

export interface ThemeColors {
    personal: ColorType;
    flow: ColorType;
}